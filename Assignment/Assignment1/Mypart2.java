
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.nio.Buffer;

import javax.swing.*;

public class Mypart2 extends Mypart1 {

	public void showVideo(String[] args){
		// read params from cmd
		// System.out.printf("params: %s, %s, %s", args[0], args[1], args[2]);
		int n;
		double revolutionPerSec;
		double framePerSec;
		double scaleFactor;
		boolean ifAntiAlias;
		if (args.length >= 1) {
			n = Integer.parseInt(args[0]);
		}
		else {
			n = 10;
		}
		if (args.length >= 2) {
			revolutionPerSec = Double.parseDouble(args[1]);
		}
		else {
			revolutionPerSec = 1.0;
		}
		if (args.length >= 3) {
			framePerSec = Double.parseDouble(args[2]);
		}
		else {
			framePerSec = 1.0;
		}
		if (args.length >= 4) {
			scaleFactor = Double.parseDouble(args[3]);
		}
		else {
			scaleFactor = 1.0;
		}
		height2 = (int)(Math.round(height1 / scaleFactor));
		width2 = (int)(Math.round(width1 / scaleFactor));
		if (args.length >= 5) {
			ifAntiAlias = Integer.parseInt(args[4]) == 1;
		}
		else {
			ifAntiAlias = false;
		}
		double fastFramePerSec = Math.max(revolutionPerSec * 2.5, 30);
		
		img1 = newImg(height1, width1);
		img2 = newImg(height1, width1);
		scaledImg2 = newImg(height2, width2);

		// Use labels to display the images/videos
		frame = new JFrame();
		GridBagLayout gLayout = new GridBagLayout();
		frame.getContentPane().setLayout(gLayout);

		JLabel lbText1 = new JLabel("Original video (Left)");
		lbText1.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel lbText2 = new JLabel("Video after modification (Right)");
		lbText2.setHorizontalAlignment(SwingConstants.CENTER);

		lbIm1 = new JLabel(new ImageIcon(img1));
		lbIm2 = new JLabel(new ImageIcon(scaledImg2));
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 0;
		frame.getContentPane().add(lbText1, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0.5;
		c.gridx = 1;
		c.gridy = 0;
		frame.getContentPane().add(lbText2, c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 1;
		frame.getContentPane().add(lbIm1, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 1;
		frame.getContentPane().add(lbIm2, c);

		frame.pack();
		frame.setVisible(true);

		// display videos
		long syncStartTime = System.currentTimeMillis() + 3 * 1000;
		ImgThread imgThread1 = new ImgThread(revolutionPerSec, fastFramePerSec, img1, height1, width1, lbIm1, n, syncStartTime, 1.0, false, null, height1, width1); 
		ImgThread imgThread2 = new ImgThread(revolutionPerSec, framePerSec, scaledImg2, height2, width2, lbIm2, n, syncStartTime, scaleFactor, ifAntiAlias, img2, height1, width1); 
        imgThread1.setName("img1");
        imgThread2.setName("img2");
        imgThread1.start();     
        imgThread2.start();     
	}

	public class ImgThread extends Thread{

		double anglePerFrame;
		double framePerSec;
		double skipTicks;
		long nextTick;
		long sleepTime;
		double revolutionPerSec;
		BufferedImage img;
		int height;
		int width;
		JLabel lbIm;
		int n;
		long syncStartTime;
		boolean ifSync = false;
		double scaleFactor;
		Boolean ifAntiAlias;
		BufferedImage refImg;
		int refHeight;
		int refWidth;

		public ImgThread(double revolutionPerSec, double framePerSec, BufferedImage img, int height, int width, JLabel lbIm, int n, long syncStartTime, double scaleFactor, boolean ifAntiAlias, BufferedImage refImg, int refHeight, int refWidth){
			this.revolutionPerSec = revolutionPerSec;
			this.framePerSec = framePerSec;
			this.skipTicks = 1000 / framePerSec;
			this.nextTick = System.currentTimeMillis();
			this.sleepTime = 0;
			this.anglePerFrame = 2.0 * Math.PI * revolutionPerSec / framePerSec;
			this.img = img;
			this.height = height;
			this.width = width;
			this.lbIm = lbIm;
			this.n = n;
			this.syncStartTime = syncStartTime;
			this.scaleFactor = scaleFactor;
			this.ifAntiAlias = ifAntiAlias;
			if (ifAntiAlias) {
				framePerSec = Math.max(framePerSec, revolutionPerSec * 2.5);
			}
			this.refImg = refImg;
			this.refHeight = refHeight;
			this.refWidth = refWidth;
		}

		public void run() {
			if (ifSync) {
				// wait and sync
				long sl = syncStartTime - System.currentTimeMillis();
				if (sl > 0) {
					try{
						sleep(sl);
					} catch(Exception e) {
					}
				}
			}

			int i = 0;
			while (true) {
				if (refImg == null) {
					resetImg(img, height, width);
					drawStarLines(img, width, height, n, -anglePerFrame * i, true); // clockwise
					lbIm.repaint();
				}
				else {
					resetImg(img, height, width);
					resetImg(refImg, refHeight, refWidth);
					drawStarLines(refImg, refWidth, refHeight, n, -anglePerFrame * i, true); // clockwise
					BufferedImage filteredImg;
					if (ifAntiAlias && scaleFactor != 1.0) {
						filteredImg = newImg(refHeight, refWidth);
						averageFilter(refImg, filteredImg, refHeight, refWidth, 3, 3);
						downsample(filteredImg, refHeight, refWidth, img, height, width, scaleFactor);
					}
					else if (ifAntiAlias && scaleFactor == 1.0) {
						img.setData(refImg.copyData(null));
					}
					else if (!ifAntiAlias && scaleFactor != 1.0) {
						downsample(refImg, refHeight, refWidth, img, height, width, scaleFactor);
					}
					else {
						img.setData(refImg.copyData(null));
					}
					lbIm.repaint();
				}
				i += 1;

				nextTick += skipTicks;
				sleepTime = nextTick - System.currentTimeMillis();
				if(sleepTime >= 0) {
					try{ 
						sleep(sleepTime);
					} catch(Exception e) {
					}
				}
				else {
					System.out.println("oops");
				}
			}
		}
	}

	public static void main(String[] args) {
		Mypart2 ren = new Mypart2();
		ren.showVideo(args);
	}

}