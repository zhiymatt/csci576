
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.nio.Buffer;

import javax.swing.*;


public class Mypart1 {

	JFrame frame;
	JLabel lbIm1;
	JLabel lbIm2;
	BufferedImage img1;
	BufferedImage img2;
	BufferedImage scaledImg2;
	int width1 = 512;
	int height1 = 512;
	int width2;
	int height2;

	// Draws a black line on the given buffered image from the pixel defined by (x1, y1) to (x2, y2)
	// java awt coordinate axis
	public void drawLine(BufferedImage image, int x1, int y1, int x2, int y2, boolean ifMark) {
		Graphics2D g = image.createGraphics();
		if (ifMark) {
			g.setColor(Color.RED);
		}
		else {
			g.setColor(Color.BLACK);
		}
		g.setStroke(new BasicStroke(1));
		g.drawLine(x1, y1, x2, y2);
		g.drawImage(image, 0, 0, null);
	}

	public void drawStarLines(BufferedImage image, int width, int height, int n, double angleStart, boolean ifMarkLine) {
		int centerX = width / 2;
		int centerY = height / 2;
		double angleInterval = 2.0 * Math.PI / n;
		double angle;
		int xx, yy; // java awt coordinate axis
		int len = Math.max(width, height);
		for (int i = 0; i < n; i++) {
			angle = angleStart + angleInterval * i;
			xx = (int)(Math.round(len * Math.cos(angle)));
			yy = -(int)(Math.round(len * Math.sin(angle)));
			drawLine(image, centerX, centerY, centerX + xx, centerY + yy, ifMarkLine && i == 0);
		}
	}

	public BufferedImage newImg(int height, int width) {
		// Initialize a plain white image
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		resetImg(image, height, width);
		return image;
	}

	public void resetImg(BufferedImage image, int height, int width) {
		int ind = 0;

		for(int y = 0; y < height; y++){

			for(int x = 0; x < width; x++){

				// byte a = (byte) 255;
				byte r = (byte) 255;
				byte g = (byte) 255;
				byte b = (byte) 255;

				int pix = 0xff000000 | ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
				//int pix = ((a << 24) + (r << 16) + (g << 8) + b);
				image.setRGB(x,y,pix);
				ind++;
			}
		}
	}

	public void downsample(BufferedImage image1, int h1, int w1, BufferedImage image2, int h2, int w2, double sf) {
		for (int i = 0; i < h2; i++) {
			for (int j = 0; j < w2; j++) {
				int ii = (int)(Math.round(i * sf));
				int jj = (int)(Math.round(j * sf));
				image2.setRGB(i, j, image1.getRGB(ii, jj));
			}
		}
	}

	public void averageFilter(BufferedImage image1, BufferedImage image2, int h, int w, int areaH, int areaW) {
		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				int accR = 0;
				int accG = 0;
				int accB = 0;
				for(int ki = 0; ki < areaH; ki++) {
					for(int kj = 0; kj < areaW; kj++) {
						try {
							Color c = new Color(image1.getRGB(j + kj - (areaW - 1) / 2, i + ki - (areaH - 1) / 2));
							accR += c.getRed();
							accG += c.getGreen();
							accB += c.getBlue();
						} catch(Exception e) {
							// System.out.println("error");
						}
					}
				}
				Color c = new Color(accR / (areaH * areaW), accG / (areaH * areaW), accB / (areaH * areaW));
				image2.setRGB(j, i, c.getRGB());
			}
		}
	}

	public void showIms(String[] args){

		// Read a parameter from command line
		// System.out.printf("params: %s, %s, %s", args[0], args[1], args[2]);
		int n;
		double scaleFactor;
		boolean ifAntiAlias;
		if (args.length >= 1) {
			n = Integer.parseInt(args[0]);
		}
		else {
			n = 10;
		}
		if (args.length >= 2) {
			scaleFactor = Double.parseDouble(args[1]);
		}
		else {
			scaleFactor = 1.0;
		}
		height2 = (int)(Math.round(height1 / scaleFactor));
		width2 = (int)(Math.round(width1 / scaleFactor));
		if (args.length >= 3) {
			ifAntiAlias = Integer.parseInt(args[2]) == 1;
		}
		else {
			ifAntiAlias = false;
		}

		// create img
		img1 = newImg(height1, width1);

		// draw lines in img
		// drawLine(img1, 0, 0, width1 - 1, 0, false);				// top edge
		// drawLine(img1, 0, 0, 0, height - 1, false);				// left edge
		// drawLine(img1, 0, height-1, width-1, height-1, false);	// bottom edge
		// drawLine(img1, width-1, height-1, width-1, 0, false); 	// right edge
		// drawLine(img, 0, 0, width-1, height-1);			// diagonal line
		drawStarLines(img1, width1, height1, n, 0.0, false);

		// anti-aliasing?
		if (ifAntiAlias) {
			img2 = newImg(height1, width1);
			averageFilter(img1, img2, height1, width1, 3, 3);
		}
		else {
			img2 = img1;
		}
		scaledImg2 = newImg(height2, width2);
		downsample(img2, height1, width1, scaledImg2, height2, width2, scaleFactor);

		// quantily aliasing
		BufferedImage newImg2 = newImg(height2, width2);
		drawStarLines(newImg2, width2, height2, n, 0.0, false);
		int diff = 0;
		int total = 0;
		for (int i = 0; i < height2; i++) {
			for (int j = 0; j < width2; j++) {
				Color scaledRGB = new Color(scaledImg2.getRGB(j, i));
				Color newRGB = new Color(newImg2.getRGB(j, i));
				if (newRGB.getRed() != 0 || newRGB.getGreen() != 0 || newRGB.getBlue() != 0) {
					total += 1;
				}
				if (Math.abs(scaledRGB.getRed() - newRGB.getRed()) + Math.abs(scaledRGB.getGreen() - newRGB.getGreen()) + Math.abs(scaledRGB.getBlue() - newRGB.getBlue()) != 0) {
					diff += 1;
				}
			}
		}
		System.out.println(1.0 * diff / total);

		// Use labels to display the images
		frame = new JFrame();
		GridBagLayout gLayout = new GridBagLayout();
		frame.getContentPane().setLayout(gLayout);

		JLabel lbText1 = new JLabel("Original image (Left)");
		lbText1.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel lbText2 = new JLabel("Image after modification (Right)");
		lbText2.setHorizontalAlignment(SwingConstants.CENTER);
		lbIm1 = new JLabel(new ImageIcon(img1));
		lbIm2 = new JLabel(new ImageIcon(scaledImg2));
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 0;
		frame.getContentPane().add(lbText1, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0.5;
		c.gridx = 1;
		c.gridy = 0;
		frame.getContentPane().add(lbText2, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 1;
		frame.getContentPane().add(lbIm1, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 1;
		frame.getContentPane().add(lbIm2, c);

		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Mypart1 ren = new Mypart1();
		ren.showIms(args);
	}

}