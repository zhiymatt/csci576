import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.nio.Buffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Random;

import javax.swing.*;


public class MyCompression {

    public static final int WIDTH = 352;
    public static final int HEIGHT = 288;

    public static final boolean DEBUG = false;

    public boolean isRGB = true;
    BufferedImage oriImg;
    BufferedImage comImg;

    public class KMeans {
        int k;
        int[][] centers;
        int[][] vecs;
        int[] belongs;
        int vecLen;

        public static final int MAX_ITER = 10000;

        public KMeans(int k, int[][] vecs) {
            this.k = k;
            this.vecs = vecs;
            this.vecLen = vecs[0].length;
            this.centers = new int[k][];
            this.belongs = new int[vecs.length];
            for (int i = 0; i < k; i++) {
                this.centers[i] = new int[vecLen];
            }
            this.vecs = vecs;
        }

        public void initCenters() {
        	Random random = DEBUG? new Random(11) : new Random(System.currentTimeMillis());
            for (int i = 0; i < centers.length; i++) {
            	int tmpId = random.nextInt(Integer.MAX_VALUE) % (vecs.length);
            	int[] tmpVec = vecs[tmpId];
                for (int j = 0; j < vecLen; j++) {
                	centers[i][j] += tmpVec[j];
                	belongs[tmpId] = i;
                }
            };
        }

        public boolean reAssign() {
        	boolean stop = true;
        	double cost = 0;
            for (int i = 0; i < vecs.length; i++) {
                double minDistance = eucDis(vecs[i], centers[belongs[i]]);
                for (int j = 0; j < k; j++) {
                    double d = eucDis(vecs[i], centers[j]);
                    if (d < minDistance) {
                        minDistance = d;
                        belongs[i] = j;
                        stop = false;
                    }
                }
                cost += minDistance;
            }
            if (DEBUG) {
            	int[] debugCount = new int[k];
            	for (int t = 0; t < belongs.length; t++) {
            		debugCount[belongs[t]]++;
            	}
            	for (int t = 0; t < k; t++) {
            		System.out.println("cluster #vec: " + debugCount[t]);
            	}
            }
            System.out.println("cost: " + cost);
            return stop;
        }

        public double eucDis(int[] a, int[] b) {
            double cal = 0;
            for (int i = 0; i < vecLen; i++) {
                double tmp = (double)(a[i] - b[i]);
                cal += (tmp * tmp);
            }
            return cal;
        }

        public void reCenter() {
            int[] count = new int[k];
            int[][] centersAcc = new int[k][];
            for (int i = 0; i < k; i++) {
                centersAcc[i] = new int[vecLen];
            }
            for (int i = 0; i < vecs.length; i++) {
                count[belongs[i]]++;
                for (int j = 0; j < vecLen; j++) {
                    centersAcc[belongs[i]][j] += vecs[i][j];
                }
            }
            for (int i = 0; i < k; i++) {
                for (int j = 0; j < vecLen; j++) {
                    centers[i][j] = (int)(centersAcc[i][j] / count[i]);
                }
            }
        }

        public int[][] getMappedVecs() {
        	int[][] mappedVecs = new int[vecs.length][];
        	for (int i = 0; i < belongs.length; i++) {
        		mappedVecs[i] = centers[belongs[i]];
        	}
            return mappedVecs;
        }
        
        public void _train() {
            boolean stop = false;
            int i = 0;
            initCenters();
            while (!stop && i < MAX_ITER) {
                stop = reAssign();
                reCenter();
                System.out.println("iter " + i);
                i++;
            }
        }
        
        public void train() {
        	while (true) {
        		try {
        			_train();
        			break;
        		}
        		catch(Exception e) {
        			e.printStackTrace();
        			System.out.println("redo training");
        		}
        	}
        	System.out.println("finish training");
        }
    }
    
    public int[][] createVecs(byte[] imgData, int mode, boolean isRGB) {
        // mode 1 for 2*1, mode 2 for 2*2, mode 3 for 4*4
        int chanLen = isRGB ? 3 : 1; 
        int vecLen = 0;
        int xStep = 0;
        int yStep = 0;
        if (mode == 1) {
            vecLen = 2 * chanLen;
            xStep = 2;
            yStep = 1;
        }
        else if (mode == 2) {
            vecLen = 2 * 2 * chanLen;
            xStep = 2;
            yStep = 2;
        }
        else if (mode == 3) {
            vecLen = 4 * 4 * chanLen;
            xStep = 4;
            yStep = 4;
        }
        else {
            ;
        }
        int[][] vecs = new int[WIDTH * HEIGHT / yStep / xStep][vecLen];
        int i = 0;
        for (int y = 0; y < HEIGHT; y += yStep) {
            for (int x = 0; x < WIDTH; x += xStep) {
                int j = 0;
                for (int yi = 0; yi < yStep; yi++) {
                    for (int xi = 0; xi < xStep; xi++) {
                        for (int c = 0; c < chanLen; c++) {
                            int ind = (y + yi) * WIDTH + (x + xi);
                            vecs[i][j * chanLen + c] = imgData[ind + c * HEIGHT * WIDTH] & 0xFF;
                        }
                        j++;
                    }
                }
                i++;
            }
        }
        return vecs;
    }

    public byte[] reverseCreateVecs(int[][] vecs, int mode, boolean isRGB) {
        // mode 1 for 2*1, mode 2 for 2*2, mode 3 for 4*4
        int chanLen = isRGB ? 3 : 1; 
        int vecLen = 0;
        int xStep = 0;
        int yStep = 0;
        if (mode == 1) {
            vecLen = 2 * chanLen;
            xStep = 2;
            yStep = 1;
        }
        else if (mode == 2) {
            vecLen = 2 * 2 * chanLen;
            xStep = 2;
            yStep = 2;
        }
        else if (mode == 3) {
            vecLen = 4 * 4 * chanLen;
            xStep = 4;
            yStep = 4;
        }
        else {
            ;
        }
        byte[] imgData = new byte[WIDTH * HEIGHT * chanLen];
        int i = 0;
        for (int y = 0; y < HEIGHT; y += yStep) {
            for (int x = 0; x < WIDTH; x += xStep) {
                int j = 0;
                for (int yi = 0; yi < yStep; yi++) {
                    for (int xi = 0; xi < xStep; xi++) {
                        for (int c = 0; c < chanLen; c++) {
                            int ind = (y + yi) * WIDTH + (x + xi);
                            imgData[ind + c * HEIGHT * WIDTH] = (byte)vecs[i][j * chanLen + c];
                        }
                        j++;
                    }
                }
                i++;
            }
        }
        return imgData;
    }

    public void showIms(String[] args) {
        String imgFileName = args[0];
        int k = Integer.parseInt(args[1]);
        int mode = Integer.parseInt(args[2]);
        isRGB = imgFileName.endsWith("rgb");
        try {
            byte[] imgData = readImg(imgFileName);
            if (isRGB) {
                oriImg = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_BGR);
                comImg = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_BGR);
            }
            else {
            	oriImg = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_BYTE_GRAY);
            	comImg = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_BYTE_GRAY);
            }
            if (DEBUG) {
                byte[] imgDataNew = reverseCreateVecs(createVecs(imgData, mode, isRGB), mode, isRGB);
                if (imgData.length != imgDataNew.length) {
                    System.out.println("Reversion fails!");
                }
                else {
                    found: {
                        for (int i = 0; i < imgData.length; i ++) {
                            if (imgData[i] != imgDataNew[i]) {
                                System.out.println("Reversion fails!");
                                break found;
                            }
                        }
                        System.out.println("Reversion succeed!");
                    }
                }
            }
            setImg(oriImg, imgData, isRGB);
            KMeans kMeans = new KMeans(k, createVecs(imgData, mode, isRGB));
            kMeans.train();
            int[][] compressedVecs = kMeans.getMappedVecs();
            setImg(comImg, reverseCreateVecs(compressedVecs, mode, isRGB), isRGB);
            displayImgs(oriImg, comImg);
            System.out.println("No problem!");
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void displayImgs(BufferedImage oriImg, BufferedImage comImg) {
        JFrame frame = new JFrame();
        GridBagLayout gLayout = new GridBagLayout();
		frame.getContentPane().setLayout(gLayout);

		JLabel lbText1 = new JLabel("Original image (Left)");
		lbText1.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel lbText2 = new JLabel("Image after modification (Right)");
		lbText2.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel oriImgLabel = new JLabel(new ImageIcon(oriImg));
		JLabel comImgLabel = new JLabel(new ImageIcon(comImg));
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 0;
		frame.getContentPane().add(lbText1, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0.5;
		c.gridx = 1;
		c.gridy = 0;
		frame.getContentPane().add(lbText2, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 1;
		frame.getContentPane().add(oriImgLabel, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 1;
		frame.getContentPane().add(comImgLabel, c);

        frame.pack();
        frame.setVisible(true);
    }

    public byte[] readImg(String imgFileName) throws IOException {
        Path path = Paths.get(imgFileName);
        byte[] imgData = Files.readAllBytes(path);
        // System.out.println(Arrays.toString(imgData));
        return imgData;
    }

    public void setImg(BufferedImage img, byte[] imgData, boolean isRGB) {
        if (isRGB) {
            int i = 0;
            int rOffset = 0;
            int gOffset = imgData.length / 3;
            int bOffset = imgData.length / 3 * 2;
            for(int y = 0; y < HEIGHT; y++){
                for(int x = 0; x < WIDTH; x++){
                    if (isRGB) {
                        byte r = imgData[i + rOffset];
                        byte g = imgData[i + gOffset];
                        byte b = imgData[i + bOffset];
                        int pix = 0xff000000 | ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
                        img.setRGB(x, y, pix);
                        i++;
                    }
                    else {
                        byte pix = imgData[i];
                        
                        i++;
                    }
                }
            }
        }
        else {
            WritableRaster raster = img.getRaster();
            raster.setDataElements(0, 0, WIDTH, HEIGHT, imgData);
        }
    }

	public static void main(String[] args) {
		MyCompression ren = new MyCompression();
        ren.showIms(args);
	}
}