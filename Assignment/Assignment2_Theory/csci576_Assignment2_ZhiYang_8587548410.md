

# CSCI 576 Assignment2

### Zhi Yang, 8587548410

 

### Question 1: Color Theory

1. ​


1. $$
   Because\ x = X / (X + Y + Z), y = Y / (X + Y + Z), z = Z / (X + Y + Z),\\
   [(1 - x - y) / y] Y = [Z / (X + Y + Z) / y] Y = (Z / Y) Y = Z
   $$

2. ​

   - Basically, it will work, because most of the colors are inside the gamut. However, this algorithm will project each out-of-gamut color perpendicularly onto the nearest edge of the gamut, which may change the color, for example, out-of-gamut blues become slightly purple.
   - This algorithm will work better on a cartoon image, because cartoon image doesn't care about accuracy of colors. This algorithm will change some of the colors in the real image, so it's not suitable for a real image.
   - Project all the colors towards the neutral point (white point inside the gamut) until all the colors are within the gamut. During the projection, we need to minimizing changes in the relationships between different colors and try to keep highly saturated colors as saturated as possible (ie out-of-gamut colors change more than in-gamut colors).



### Question 2: Generic Compression

- Average code len = 0.625 * 1 + 0.125 * 2 + 0.25 * 2 = 1.375


- | alphabet | code |
  | -------- | ---- |
  | A        | 0    |
  | B        | 10   |
  | C        | 11   |

  Four.

  1. A: 0, B: 10, C: 11
  2. A: 0, B: 11, C: 10
  3. A: 1, B: 00, C: 01
  4. A: 1, B: 01, C: 00

- To represent three symbol with prefix code, we need three codeworks of length 1, 2 and 2. Calculate the average code length of all the possible distinct permutation: 
  $$
  1 * 0.625 + 2 * 0.125 + 2 * 0.25 = 1.375 \\
  2 * 0.625 + 1 * 0.125 + 2 * 0.25 = 1.875\\
  2 * 0.625 + 2 * 0.125 + 1 * 0.25 = 1.75 \\
  $$
  It's obviously that the code I have defined is optimal prefix code.



### Question 3: Entropy Coding

- Entropy
  $$
  H = -P(X) * \log_2 P(X) - P(Y) * \log_2 P(Y) = -x^k \log_2 x^k - (1 - x ^ k) \log_2 (1 - x ^ k)
  $$
  When k = 2:

  ![Q31](C:\Users\miylo\GitLab\csci576\Assignment\Assignment2_Theory\Q31.png)


- When k = 2, H becomes a minimum when x = 0 or x = 1 or x = -1.

- When H is a minimum,

- $$
  \begin{equation}
  x(k)
  \begin{cases}
  -1\ or\ 0\ or\ 1& \text{if k % 2 = 0} \\
  0\ or\ 1& \text{otherwise}
  \end{cases}
  \end{equation}
  $$

- When k = 2, H becomes a maximum when x = $-1/\sqrt{2}$ or x = $+1/\sqrt{2}$ .

- When H is a maximum,

- $$
  \begin{equation}
  x(k)
  \begin{cases}
  \frac{1}{\sqrt[k]{2}}\ or\ \frac{-1}{\sqrt[k]{2}}& \text{if k % 2 = 0} \\
  \frac{1}{\sqrt[k]{2}}& \text{otherwise}
  \end{cases}
  \end{equation}
  $$


### Question 4: DCT Coding

- ```
  [[10  2  0  0  0  0  0  0]
   [ 1  1 -1  0  0  0  0  0]
   [ 0  0  0  1  0  0  0  0]
   [ 0  0  0  0  0  0  0  0]
   [ 0  0  0  0  0  0  0  0]
   [ 0  0  0  0  0  0  0  0]
   [ 0  0  0  0  0  0  0  0]
   [ 0  0  0  0  0  0  0  0]]
  ```

  ![Q41](C:\Users\miylo\GitLab\csci576\Assignment\Assignment2_Theory\Q41.png)

- ```
  [10,  2,  1,  0,  1,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0, 1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0]
  ```

  ![Q41](C:\Users\miylo\GitLab\csci576\Assignment\Assignment2_Theory\Q42.png)


- ```
  <0, 2><2>
  <0, 1><1>
  <1, 1><1>
  <2, 1><-1>
  <9, 1><1>
  <EOB>
  ```

- ```
  01 10 00 1 1100 1 11100 0 111111001 1 1010
  ```

- ```
  len of AC bit stream: 32;
  size of original 8x8 block: 8*8*8=512;
  because previous block DC value is not given, we ignore the size of DPCM stream;
  compression ratio CR = 512 : 32 = 16 : 1
  ```

  ​

