package csci576;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.DataLine.Info;

import csci576.Params;

public class Soundtrack {
	
	public String videoName;
	
	public int fromFrameIndex;
	
	private int audioBufferSize;

	private SourceDataLine dataLine = null;
	
	private int sampleIndex;
	private int readBytes = 0;
	
	private AudioInputStream audioInputStream;
	
	byte[] audioBuffer;
	
	public Soundtrack(String videoName, int fromFrameIndex) {
		initSoundtrack(videoName, fromFrameIndex);
    }
    
    public void redirect(String videoName, int fromFrameIndex) {
    	initSoundtrack(videoName, fromFrameIndex);
    }
	
	private void initSoundtrack(String videoName, int fromFrameIndex) {		
		this.videoName = videoName;
		this.fromFrameIndex = fromFrameIndex;
		this.sampleIndex = fromFrameIndex;
		
		
		Path path = Paths.get(".", "videos", videoName, videoName + ".wav");
		
		FileInputStream inputStream = null;
		try {
		    inputStream = new FileInputStream(path.toFile());
			InputStream bufferedIn = new BufferedInputStream(inputStream);
		    audioInputStream = AudioSystem.getAudioInputStream(bufferedIn);
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Obtain the information about the AudioInputStream
		AudioFormat audioFormat = audioInputStream.getFormat();
		Info info = new Info(SourceDataLine.class, audioFormat);
		
		audioBufferSize = (int) (audioFormat.getSampleRate() * audioFormat.getFrameSize() / Params.FRAME_RATE);

		// opens the audio channel
		try {
		    dataLine = (SourceDataLine) AudioSystem.getLine(info);
		    dataLine.open(audioFormat, audioBufferSize);
		} catch (LineUnavailableException e) {
		    e.printStackTrace();
		}
		
		readBytes = 0;
		audioBuffer = new byte[audioBufferSize];
		
	    try {
			inputStream.skip((fromFrameIndex - 1) * audioBufferSize);
		} catch (IOException e) {
			e.printStackTrace();
		}

		dataLine.start();
	}
	
	public boolean available() {
		return (readBytes != -1);
	}
	
	public void cache() {
		try {
			readBytes = audioInputStream.read(audioBuffer, 0, audioBuffer.length);
			if (readBytes >= 0){
				/* '.write' method doesn't wait until all the data has finished playing.
				 * If you try to write more data than the buffer will hold, the method 
				 * blocks (doesn't return) until all the data you requested has actually 
				 * been placed in the buffer.
				 */
			    dataLine.write(audioBuffer, 0, readBytes);
			    sampleIndex++;
			    //System.out.println("sampleIndex: " + sampleIndex);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void drain() {
	    // plays what's left and and closes the audioChannel
	    dataLine.drain();
	    dataLine.close();
	}

}
