package csci576;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class VideoFile {

	String videoName;

	public VideoFile(String videoName) throws FileNotFoundException {
		this.videoName = videoName;
	}

	public BufferedImage readFrame(int frameIndex) throws IOException {
		Path path = Paths.get(".", "videos", videoName, videoName + String.format("%04d", frameIndex) + ".rgb");
		byte[] frameData = Files.readAllBytes(path);
		return byteArr2BufImg(frameData);
	}

	public BufferedImage byteArr2BufImg(byte[] imgData) {
		BufferedImage bufImg = new BufferedImage(Params.WIDTH, Params.HEIGHT, BufferedImage.TYPE_INT_BGR);
		int i = 0;
		int rOffset = 0;
		int gOffset = imgData.length / 3;
		int bOffset = imgData.length / 3 * 2;
		for (int y = 0; y < Params.HEIGHT; y++) {
			for (int x = 0; x < Params.WIDTH; x++) {
				byte r = imgData[i + rOffset];
				byte g = imgData[i + gOffset];
				byte b = imgData[i + bOffset];
				int pix = 0xff000000 | ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
				bufImg.setRGB(x, y, pix);
				i++;
			}
		}
		return bufImg;
	}

}
