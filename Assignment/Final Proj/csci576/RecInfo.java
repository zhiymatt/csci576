package csci576;

import java.awt.Color;
import java.awt.Rectangle;

public class RecInfo {
	public Rectangle rectangle;
	public String target;
	public int targetFrame;
	public Color color;
	public RecInfo(Rectangle rec, String target, int targetFrame, String color) {
		this.rectangle = rec;
		this.target = target;
		this.targetFrame = targetFrame;
		switch(color) {
		case "Red": this.color = Color.RED; break;
		case "Green": this.color = Color.GREEN; break;
		case "Blue": this.color = Color.BLUE; break;
		case "White": this.color = Color.WHITE; break;
		case "Black": this.color = Color.BLACK; break;
		default: this.color = Color.WHITE;
		}
	}
}
