package csci576;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Paths;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import csci576.Screen;
import javafx.util.Pair;

/* Video Format (video.avi): 5 minutes long, 30 frames per second, 352x288 resolution
 * 
 * Frame Format (frame.rgb): Same as Assignment 2 where the resolution is 352x288 with 
 * each .rgb file containing 352*288 red bytes, followed by 352*288 green bytes, 
 * followed by 352*288 blue bytes. The frames are given in sequential order and there 
 * are 30 frames per second of video (9000 frames in total for each video).
 *
 * Audio Format (audio.wav): 5 minutes long, 16 bits per sample, sampling rate of 
 * 44,100 samples per second
 */


public class HyperlinkEditor implements Runnable {

    private Screen sourceScreen;
    private Screen targetScreen;
    
    private String sourceVideoName;
    private String targetVideoName;

    private JFrame jFrame;
    private HashMap<String, FrameRec> frames;

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new HyperlinkEditor());
    }

    public HyperlinkEditor() {
    }
    
    public void readFrame(String sourceVideoName) {
        // read and preprocess rectangles
    	this.frames = new HashMap<>();
    	try {
			BufferedReader br = new BufferedReader(new FileReader(new File(sourceVideoName+".txt")));
			String current;
			while((current = br.readLine()) != null) {
				String[] parse = current.split(":");
				Rectangle s = new Rectangle((int)Double.parseDouble(parse[2]), (int)Double.parseDouble(parse[3]),(int)Double.parseDouble(parse[4]),(int)Double.parseDouble(parse[5]));
				Rectangle e = new Rectangle((int)Double.parseDouble(parse[7]),(int)Double.parseDouble(parse[8]),(int)Double.parseDouble(parse[9]),(int)Double.parseDouble(parse[10]));		
				frames.put(parse[0], new FrameRec(s, e, Integer.valueOf(parse[1]), Integer.valueOf(parse[6]), parse[11], Integer.valueOf(parse[12]),parse[13]));
			}
			br.close();
		} catch (FileNotFoundException e) {
			// e.printStackTrace();
			System.out.println("FILE NOT FOUND!");
		} catch (IOException e) {
			e.printStackTrace();
            System.out.println("IOEXCEPTION!");
		}
    }
    

    @Override
    public void run() {
        
    	jFrame = new JFrame("VIDEO");
    	jFrame.setLayout(new BoxLayout(jFrame.getContentPane(), BoxLayout.Y_AXIS));
    	jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
    	// import panel
    	
    	JPanel importPanel = new JPanel();
    	importPanel.setLayout(new GridBagLayout());
    	GridBagConstraints c = new GridBagConstraints();
    	c.fill = GridBagConstraints.HORIZONTAL;
    	
        JPanel sourceImportPanel = new JPanel();
        JPanel targetImportPanel = new JPanel();
    	
        JLabel sourceTextLabel = new JLabel("Source Video");
        JButton sourcePathButton = new JButton("Import");
        sourceImportPanel.add(sourceTextLabel);
        sourceImportPanel.add(sourcePathButton);
        
        JLabel TargetTextLabel = new JLabel("Target Video");
        JButton targetPathButton = new JButton("Import");
        targetImportPanel.add(TargetTextLabel);
        targetImportPanel.add(targetPathButton);
        
        c.weightx = 0.5;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        importPanel.add(sourceImportPanel, c);
        
        c.weightx = 0.5;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 0;
        importPanel.add(targetImportPanel, c);
        
        jFrame.add(importPanel);
        
        // hyperlink panel
        
        JPanel hyperlinkPanel = new JPanel();
        
        DefaultListModel<String> recModel = new DefaultListModel<>();
        JList<String> recList = new JList<>(recModel);
        JScrollPane recListPane = new JScrollPane(recList);
        recListPane.setPreferredSize(new Dimension(250, 70));
        recListPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        

        JButton deleteButton = new JButton("Delete");
        JButton unselect = new JButton("Unselect");
        JButton saveButton = new JButton("Save and Quit");
        
        hyperlinkPanel.add(recListPane);
        hyperlinkPanel.add(unselect);
        hyperlinkPanel.add(deleteButton);
        hyperlinkPanel.add(saveButton);
        
        jFrame.add(hyperlinkPanel);
        
        // video panel
        
        JPanel videoPanel = new JPanel();
        JPanel sourceVideoPanel = new JPanel();
        sourceVideoPanel.setLayout(new BoxLayout(sourceVideoPanel, BoxLayout.Y_AXIS));
        JPanel targetVideoPanel = new JPanel();
        targetVideoPanel.setLayout(new BoxLayout(targetVideoPanel, BoxLayout.Y_AXIS));
        
		JTextField sourceFrameSliderText = new JTextField();
		JSlider sourceFrameSlider = new JSlider(JSlider.HORIZONTAL,
		        Params.FRAME_INDEX_MIN, Params.FRAME_INDEX_MAX, Params.FRAME_INDEX_INIT);
		sourceFrameSlider.setMinorTickSpacing(100);
		sourceFrameSlider.setPaintTicks(true);
		sourceFrameSlider.setPaintLabels(true);
    	sourceScreen = new Screen(null, sourceFrameSlider, sourceFrameSliderText);
    	sourceVideoPanel.add(sourceScreen);
    	sourceVideoPanel.add(sourceFrameSliderText);
    	sourceVideoPanel.add(sourceFrameSlider);
        
		JTextField targetFrameSliderText = new JTextField();
		JSlider targetFrameSlider = new JSlider(JSlider.HORIZONTAL,
		        Params.FRAME_INDEX_MIN, Params.FRAME_INDEX_MAX, Params.FRAME_INDEX_INIT);
		targetFrameSlider.setMinorTickSpacing(100);
		targetFrameSlider.setPaintTicks(true);
		targetFrameSlider.setPaintLabels(true);
    	targetScreen = new Screen(null, targetFrameSlider, targetFrameSliderText);
    	targetVideoPanel.add(targetScreen);
    	targetVideoPanel.add(targetFrameSliderText);
    	targetVideoPanel.add(targetFrameSlider);		
    	
        videoPanel.add(sourceVideoPanel, BorderLayout.WEST);
        videoPanel.add(targetVideoPanel, BorderLayout.EAST);
        jFrame.add(videoPanel);
		
        // listeners
		
		unselect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				recList.clearSelection();			
			}
		});
		
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!recList.isSelectionEmpty()) {
					String selectedKey = recList.getSelectedValue();
					frames.remove(selectedKey);
					recModel.removeElement(selectedKey);
					sourceScreen.reRender(frames);
				}
			}
			
		});
		
		saveButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				File rectFile = new File(sourceVideoName + ".txt");
				try {
					FileOutputStream output = new FileOutputStream(rectFile);
					BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(output));
					for(String key : frames.keySet()) {
						FrameRec current = frames.get(key);
						bw.write(key+":"+current.getStartFrame()
								+":"+current.getStart().getX()
								+":"+current.getStart().getY()
								+":"+current.getStart().getWidth()
								+":"+current.getStart().getHeight()
								+":"+current.getEndFrame()
								+":"+current.getEnd().getX()
								+":"+current.getEnd().getY()
								+":"+current.getEnd().getWidth()
								+":"+current.getEnd().getHeight()
								+":" + current.getTargetName()
								+":"+current.getTargetFrame()
								+":"+current.getColor());
						bw.newLine();
					}
					bw.close();
					output.close();
					System.exit(0);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});

		sourceScreen.addMouseListener(new MouseAdapter() {
			Point start;
			Point end;
			public void mouseClicked(MouseEvent e) {
				Pair<String, Integer> target = sourceScreen.getTarget(e.getPoint(), sourceFrameSlider.getValue());
				if(target != null) {
					targetVideoName = target.getKey();
					targetScreen.redirect(targetVideoName, target.getValue());
					targetFrameSlider.setValue(target.getValue());
					targetScreen.repaint();
				}
			}
			
    		public void mousePressed(MouseEvent e) {
    			start = e.getPoint();
    		}
    		
    		public void mouseReleased(MouseEvent e) {
    			end = e.getPoint();
    			int startX = (int)start.getX();
    	    	int startY = (int)start.getY();
    	    	int height = (int)(end.getY() - startY);
    	    	int width = (int)(end.getX() - startX);
    	    	Rectangle rec = new Rectangle(startX, startY, width, height);
    			if(!end.equals(start)) {
    				if(targetVideoName == null) {
    					JFrame tem = new JFrame("err");
    					JOptionPane.showMessageDialog(tem, "Target Video is Null", "Warning", JOptionPane.ERROR_MESSAGE);
    					return;
    				}
    				if(!recList.isSelectionEmpty()) {
    					frames.get(recList.getSelectedValue()).setEndFrame(sourceFrameSlider.getValue());
    					frames.get(recList.getSelectedValue()).setEndRec(rec);
    					recList.clearSelection();
    				}else {
    					sourceScreen.drawTempRec(rec);
    					JFrame dialog = new JFrame("frame name input");
    					String dialogName = JOptionPane.showInputDialog(dialog, "Please enter the name of your rectangle");
    					while(frames.containsKey(dialogName)) {
    						dialogName = JOptionPane.showInputDialog(dialog, "Name Conflict! Please enter a new name.");
    					}
    					String[] opts = {"Red", "Green", "Blue", "White", "Black"};
    					String color_string = (String) JOptionPane.showInputDialog(null, "Choose Color", "Please select a color", JOptionPane.QUESTION_MESSAGE, null, opts, opts[0]);
    					if(dialogName != null && dialogName.length() > 0 && color_string != null && color_string.length() > 0) {
    						frames.put(dialogName, new FrameRec(rec, sourceFrameSlider.getValue(), targetVideoName, targetFrameSlider.getValue(), color_string));
    						recModel.addElement(dialogName);
    					}
    					
    				}
    				
    				sourceScreen.reRender(frames);
    			}
    		}
    	});
		
    	sourcePathButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new File("."));
				chooser.setDialogTitle("choose a file");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);
				if(chooser.showOpenDialog(sourcePathButton) == JFileChooser.APPROVE_OPTION) {
					sourceVideoName = Paths.get(chooser.getSelectedFile().getPath()).getFileName().toString();
					readFrame(sourceVideoName);
					//initialization
					recModel.clear();
					for(String key : frames.keySet()) {
						recModel.addElement(key);
					}
					sourceScreen.redirect(sourceVideoName, 1);
					sourceScreen.reRender(frames);
					sourceFrameSlider.setValue(1);
				}
			}
    		
    	});
		
		targetPathButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new File("."));
				chooser.setDialogTitle("choose a file");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);
				if(chooser.showOpenDialog(targetPathButton) == JFileChooser.APPROVE_OPTION) {
					targetVideoName = Paths.get(chooser.getSelectedFile().getPath()).getFileName().toString();
					targetScreen.redirect(targetVideoName, 1);
					targetScreen.repaint();
					targetFrameSlider.setValue(1);
					
				}
			}
    		
    	});
		
		//sourceScreen.reRender(frames);

        jFrame.pack();
        jFrame.setLocationByPlatform(true);
        jFrame.setVisible(true);
    }
    
}