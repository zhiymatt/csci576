package csci576;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import javafx.util.Pair;


public class Player implements Runnable {
	
	public String fileName;	
    private JFrame jFrame;
    private Screen screen;
    private Soundtrack soundtrack;
    private HashMap<String, FrameRec> frames;
    
    
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Player());
	}
	
	public Player() {
		
	}

	public void initWithFrames(String fileName) {
		this.fileName = fileName;
		this.frames = new HashMap<>();
    	try {
			BufferedReader br = new BufferedReader(new FileReader(new File(fileName+".txt")));
			String current;
			while((current = br.readLine()) != null) {
				String[] parse = current.split(":");
				Rectangle s = new Rectangle((int)Double.parseDouble(parse[2]), (int)Double.parseDouble(parse[3]),(int)Double.parseDouble(parse[4]),(int)Double.parseDouble(parse[5]));
				Rectangle e = new Rectangle((int)Double.parseDouble(parse[7]),(int)Double.parseDouble(parse[8]),(int)Double.parseDouble(parse[9]),(int)Double.parseDouble(parse[10]));
				frames.put(parse[0], new FrameRec(s, e, Integer.valueOf(parse[1]), Integer.valueOf(parse[6]), parse[11], Integer.valueOf(parse[12]), parse[13]));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("NO FILE FOUND --- VANILLA");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public void run() {

		// video and audio player
		screen = new Screen(null, 1);
    	//soundtrack = new Soundtrack(fileName, 1);
		
        JTextField frameIndexIndicator = new JTextField();
        
    	// create a thread to play video and audio syncly
        PlayerThread playerThread = new PlayerThread(fileName, screen, soundtrack, frameIndexIndicator); 
        playerThread.setName("PlayerThread");
		
		// set layouts, place screen and controlPanel
    	jFrame = new JFrame("Player");
    	
    	jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
    	BorderLayout bLayout = new BorderLayout();
    	jFrame.getContentPane().setLayout(bLayout);

        jFrame.add(frameIndexIndicator, BorderLayout.SOUTH);
    	
    	JPanel pathPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    	JButton pathButton = new JButton("Import");
    	pathPanel.add(pathButton);
    	
    	pathButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new File("."));
				chooser.setDialogTitle("choose a file");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);
				if(chooser.showOpenDialog(pathButton) == JFileChooser.APPROVE_OPTION) {
					fileName = Paths.get(chooser.getSelectedFile().getPath()).getFileName().toString();;
					initWithFrames(fileName);
					//initialization
					screen.redirect(fileName, 1);
					soundtrack = new Soundtrack(fileName, 1);
					screen.reRender(frames);
					playerThread.add(soundtrack, screen);
			        frameIndexIndicator.setText("Frame: " + Params.FRAME_INDEX_MIN);
				}
			}
    		
    	});
    	
    	jFrame.add(pathPanel, BorderLayout.NORTH);
    	jFrame.add(screen, BorderLayout.CENTER);
    	
    	JPanel controlPanel = new JPanel();
    	jFrame.add(controlPanel, BorderLayout.EAST);
    	controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
    	
    	// place resume, pause and stop button
    	int iconOffset = 0;
    	int iconHeight = Params.HEIGHT / 3;
    	
    	ImageIcon resumeIcon = new ImageIcon("./csci576/icon/resume.png");
		JButton resumeButton = new JButton();  
		resumeButton.setBounds(100, 100, iconHeight, iconHeight);    
		controlPanel.add(resumeButton);
		iconOffset = resumeButton.getInsets().left;
		resumeButton.setIcon(resizeIcon(resumeIcon, resumeButton.getWidth() - iconOffset, resumeButton.getHeight() - iconOffset));
		resumeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				playerThread.playerResume();
				System.out.println("resume button pressed");
			}          
		});
    	
    	ImageIcon pauseIcon = new ImageIcon("./csci576/icon/pause.png");
		JButton pauseButton = new JButton();  
		pauseButton.setBounds(100, 100, iconHeight, iconHeight);    
		controlPanel.add(pauseButton);
		iconOffset = pauseButton.getInsets().left;
		pauseButton.setIcon(resizeIcon(pauseIcon, pauseButton.getWidth() - iconOffset, pauseButton.getHeight() - iconOffset));
    	pauseButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				playerThread.playerPause();
				System.out.println("pause button press");
			}          
		});
    	
    	ImageIcon stopIcon = new ImageIcon("./csci576/icon/stop.png");
		JButton stopButton = new JButton();  
		stopButton.setBounds(100, 100, iconHeight, iconHeight);    
		controlPanel.add(stopButton);
		iconOffset = stopButton.getInsets().left;
		stopButton.setIcon(resizeIcon(stopIcon, stopButton.getWidth() - iconOffset, stopButton.getHeight() - iconOffset));
		stopButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				playerThread.playerStop();
				screen.reRender(frames);
				System.out.println("stop button press");
			}          
		});
		
		screen.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				int index = screen.getFrameIndex();
				HashMap<Integer, List<RecInfo>> map = screen.getMap();
				if(map.containsKey(index)) {
					for(RecInfo p : map.get(index)) {
						if(p.rectangle.contains(e.getPoint())) {
							screen.redirect(p.target, p.targetFrame);
							soundtrack.redirect(p.target, p.targetFrame);
							if(!p.target.equals(fileName)) {
								fileName = p.target;
								initWithFrames(fileName);
							}
							screen.reRender(frames);
						}
					}
				}
			}
			
    	});

        jFrame.pack();
        jFrame.setLocationByPlatform(true);
        jFrame.setVisible(true);
        
        // start the thread to play video and audio
        playerThread.start(); 
        
	}
	
	private static ImageIcon resizeIcon(ImageIcon icon, int resizedWidth, int resizedHeight) {
	    Image img = icon.getImage();  
	    Image resizedImage = img.getScaledInstance(resizedWidth, resizedHeight,  java.awt.Image.SCALE_SMOOTH);  
	    return new ImageIcon(resizedImage);
	}
	
}

// Icon made by Freepik from www.flaticon.com 
