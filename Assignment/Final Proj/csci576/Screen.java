package csci576;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import javafx.util.Pair;

public class Screen extends JPanel implements ChangeListener{
    
	public String videoFileName;
	
	public int fromFrameIndex;
	
	private VideoFile vf;
    
	private int frameIndex;
    
    private HashMap<Integer, List<RecInfo>> map;
    private HashMap<String, String> colors;
    //private List<Pair<Rectangle, Pair<String, Integer>>> currentList;
    
    JTextField frameSliderText;

    private static final long serialVersionUID = -8587548410L;
    
    public Screen(String videoFileName, int fromFrameIndex) {
    	initScreen(videoFileName, fromFrameIndex);
    }
    
    public void redirect(String videoFileName, int fromFrameIndex) {
    	initScreen(videoFileName, fromFrameIndex);
    }
    
    private void initScreen(String videoFileName, int fromFrameIndex) {
    	this.videoFileName = videoFileName;
    	this.fromFrameIndex = fromFrameIndex;
    	frameIndex = fromFrameIndex;
    	map = new HashMap<>();
    	if (videoFileName != null) {
        	try {
    			vf = new VideoFile(videoFileName);
    		} catch (FileNotFoundException e) {
    			System.out.println("File not found.");
    			e.printStackTrace();
    		}
    	}
        this.setBackground(Color.WHITE);
        this.setPreferredSize(new Dimension(Params.WIDTH, Params.HEIGHT));
    }

    public Screen(String videoFileName, JSlider frameSlider, JTextField frameSliderText) {
    	this.videoFileName = videoFileName;
    	this.frameSliderText = frameSliderText;
    	frameIndex = 1;
        frameSliderText.setText("Frame: " + frameIndex);
    	map = new HashMap<>();
    	if (videoFileName != null) {
        	try {
    			vf = new VideoFile(videoFileName);
    		} catch (FileNotFoundException e) {
    			System.out.println("File not found.");
    			e.printStackTrace();
    		}
    	}
    	frameSlider.addChangeListener(this);
        this.setBackground(Color.WHITE);
        this.setPreferredSize(new Dimension(Params.WIDTH, Params.HEIGHT));
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        if (videoFileName != null) {
			try {
				BufferedImage img = vf.readFrame(frameIndex);
	            g.drawImage(img, 0, 0, this);
			} catch (IOException e) {
				System.out.println("Frame not found.");
				e.printStackTrace();
			}
        }
        
        if(map.containsKey(frameIndex)) {
        	List<RecInfo>recs = map.get(frameIndex);
        	for(RecInfo p : recs) {
        		Rectangle rec = p.rectangle;
        		g.setColor(p.color);
	        	g.drawRect((int)rec.getX(), (int)rec.getY(), (int)rec.getWidth(), (int)rec.getHeight());
        	}
        }
    }

	@Override
	public void stateChanged(ChangeEvent e) {
        JSlider slider = (JSlider)e.getSource();
        if (true) { // !slider.getValueIsAdjusting()) {
            frameIndex = (int)slider.getValue();
            frameSliderText.setText("Frame: " + frameIndex);
            this.repaint();
        }
	}
	
	public void drawTempRec(Rectangle tem) {
		if(map.get(frameIndex) == null) map.put(frameIndex, new ArrayList<RecInfo>());
		map.get(frameIndex).add(new RecInfo(tem, "", -1, "white"));
		repaint();
	}
    
    public void reRender(HashMap<String, FrameRec> input) {
    	map = new HashMap<>();
    	for(String key : input.keySet()) {
    		FrameRec current = input.get(key);
    		int start = current.getStartFrame();
    		int end = current.getEndFrame();
    		Rectangle s = current.getStart();
    		Rectangle e = current.getEnd();
    		double deltaX = (end == start) ? 0 : (e.getX() - s.getX()) / (end - start);
    		double deltaY = (end == start) ? 0 : (e.getY() - s.getY()) / (end - start);
    		double prevX = s.getX() - deltaX;
    		double prevY = s.getY() - deltaY;
    		double deltaWidth = (end == start) ? 0 : (e.getWidth() - s.getWidth()) / (end - start);
    		double deltaHeight = (end == start) ? 0 : (e.getHeight() - s.getHeight()) / (end - start);
    		double prevWidth = s.getWidth() - deltaWidth;
    		double prevHeight = s.getHeight() - deltaHeight;
    		
    		for(int i = start; i <= end; i++) {
    			List<RecInfo> list = map.containsKey(i) ? map.get(i) : new ArrayList<RecInfo>();
    			double newX = prevX + deltaX;
				double newY = prevY + deltaY;
				double newWidth = prevWidth + deltaWidth;
				double newHeight = prevHeight + deltaHeight;
				list.add(new RecInfo(new Rectangle((int)newX, (int)newY, (int)newWidth, (int)newHeight), current.getTargetName(),current.getTargetFrame(), current.getColor()));
				if(!map.containsKey(i)) map.put(i, list);
				prevX = newX;
				prevY = newY;
				prevWidth = newWidth;
				prevHeight = newHeight;
    		}
    	}
    	repaint();
    }
    
    public int currentFrame() {
    	return frameIndex;
    }
    
    public int targetFrame(Point clicked) {
    	for(RecInfo p : map.get(frameIndex)) {
    		if(p.rectangle.contains(clicked)) {
    			return p.targetFrame;
    		}
    	}
    	return -1;
    }
    
    public void incFrameIndex() {
    	frameIndex++;
    	//System.out.println("frameIndex: " + frameIndex);
    }
    
    public boolean available() {
    	return (frameIndex <= Params.FRAME_INDEX_MAX);
    }
    
    public int getFrameIndex() {
    	return frameIndex;
    }
    
    public HashMap<Integer, List<RecInfo>> getMap() {
    	return map;
    }
    
    public Pair<String, Integer> getTarget(Point point, int index) {
        if (index < map.size()) {
        	for(RecInfo p : map.get(index)) {
        		if(p.rectangle.contains(point)) {
        			return new Pair<String, Integer>(p.target, p.targetFrame);
        		}
        	}
        }
    	return null;
    }
}