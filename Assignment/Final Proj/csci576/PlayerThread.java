package csci576;

import javax.swing.JTextField;

public class PlayerThread extends Thread {
	
	public String fileName;
	
	public JTextField frameIndexIndicator;
	
	private Screen screen;
	private Soundtrack soundtrack;
	
	private double skipTicks;
	private double nextTick;
	private long sleepTime;
	
	private int playerState = 0; // 0 for resume, 1 for pause, 2 for stop
	
	public PlayerThread(String fileName, Screen screen, Soundtrack soundtrack, JTextField frameIndexIndicator) {
		
		this.fileName = fileName;
		
		this.screen = screen;
		this.soundtrack = soundtrack;
		
		this.skipTicks = 1000.0 / Params.FRAME_RATE;
		this.sleepTime = 0;
		
		this.frameIndexIndicator = frameIndexIndicator;
		frameIndexIndicator.setText("Frame: ");
		
	}
	
	public void run() {
	    while (true) {
		
    		// long startUpdateTime;
    		
    		this.nextTick = System.currentTimeMillis();
    		
    		while(soundtrack == null) {
    			System.out.println("busy waiting");
    		}
    		
    		while (screen.available() && soundtrack.available()) {
    
    			if (playerState == 0) {
    				// startUpdateTime = System.currentTimeMillis();
    				soundtrack.cache();
    				
    				screen.repaint();
                    frameIndexIndicator.setText("Frame: " + screen.getFrameIndex());
    				screen.incFrameIndex();
    				
    				
    				// System.out.println("update time: " + (System.currentTimeMillis() - startUpdateTime));
    			}
    
    			nextTick += skipTicks;
    			sleepTime = (long) (nextTick - System.currentTimeMillis());
    			if(sleepTime >= 0) {
    				try{ 
    					sleep(sleepTime);
    					//System.out.println("wait: " + sleepTime);
    				} catch(Exception e) {
    				}
    			}
    			else {
    				//System.out.println("oops: " + sleepTime);
    			}
    		}
    		
    		soundtrack.drain();
	    }
		
	}
	
	public void add(Soundtrack sound, Screen screen) {
		this.soundtrack = sound;
		this.screen = screen;
	}
	
	public void playerResume() {
		playerState = 0;
	}
	
	public void playerPause() {
		playerState = 1;
	}
	
	public void playerStop() {
		screen.redirect(screen.videoFileName, 1);
		soundtrack.redirect(soundtrack.videoName, 1);
		screen.repaint();
		playerState = 1;
        frameIndexIndicator.setText("Frame: " + screen.getFrameIndex());
	}
}