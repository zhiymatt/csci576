package csci576;

import java.awt.Rectangle;

public class FrameRec {
	private Rectangle start;
	private Rectangle end;
	private int startFrame;
	private int endFrame;
	private String targetName;
	private int targetFrame;
	
	public FrameRec(Rectangle s, int startFrame, String targetName, int targetFrame) {
		this.start = s;
		this.startFrame = startFrame;
		this.endFrame = 9000;
		this.end = start;
		this.targetName = targetName;
		this.targetFrame = targetFrame;
	}
	
	public FrameRec(Rectangle s, Rectangle e, int startFrame, int endFrame, String targetName, int targetFrame) {
		this.start = s;
		this.end = e;
		this.startFrame = startFrame;
		this.endFrame = endFrame;
		this.targetFrame = targetFrame;
		this.targetName = targetName;
	}
	
	public void setEndFrame(int endFrame) {
		this.endFrame = endFrame;
	}
	
	public void setEndRec(Rectangle e) {
		this.end = e;
	}
	
	public void setTargetname(String name) {
		this.targetName = name;
	}
	
	public Rectangle getStart() {
		return start;
	}
	
	public Rectangle getEnd() {
		return end;
	}
	
	public int getStartFrame() {
		return startFrame;
	}
	
	public int getEndFrame() {
		return endFrame;
	}
	
	public int getTargetFrame() {
		return targetFrame;
	}
	
	public String getTargetName() {
		return targetName;
	}
}
