package csci576;

public class Params {
    
	public static final int WIDTH = 352;
	public static final int HEIGHT = 288;
	public static final int FRAME_RATE = 30;

	public static final int FRAME_INDEX_MIN = 1;
	public static final int FRAME_INDEX_MAX = 9000;
	public static final int FRAME_INDEX_INIT = 1;

}