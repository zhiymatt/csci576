Video Format (video.avi): 5 minutes long, 30 frames per second, 352x288 resolution

Frame Format (frame.rgb): Same as Assignment 2 where the resolution is 352x288 with each .rgb file containing 352*288 red bytes, followed by 352*288 green bytes, followed by 352*288 blue bytes. The frames are given in sequential order and there are 30 frames per second of video (9000 frames in total for each video).

Audio Format (audio.wav): 5 minutes long, 16 bits per sample, sampling rate of 44,100 samples per second